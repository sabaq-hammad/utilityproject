package com.sabaq.utilityproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.inputmethod.InputMethodManager;


import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ishaq Ahmed Khan on 3/30/2017.
 */

public class GlobalHelper {

    // Sync Adapter ke Attributes
    public long SECONDS_PER_MINUTE = 60L;
    public long SYNC_INTERVAL_IN_MINUTES = 300L;
    public long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;

    //******** create account
    // The authority for the sync adapter's content provider
    public String AUTHORITY = "com.example.android.datasync123.provider";
    // An account type, in the form of a domain name
    public String ACCOUNT_TYPE = "123example.com";
    // The account name
    public String ACCOUNT = "dummyaccount123";

    public static final String URL = "http://smart.sabaq.edu.pk/api/";
    public static final String URLAuthentication = "http://smart.sabaq.edu.pk/";
    private static ProgressDialog pDialog;
    private static AlertDialog.Builder alertDialogBuilder;
    //this separate Date from monitornigID
    public static String dateSeprater = ";";

    public static AlertDialog.Builder showAlertDialog(Context context, String title, String msg, int icon, boolean cancelable,
                                                      int posBtnText, DialogInterface.OnClickListener posBtnListner,
                                                      int negBtnText, DialogInterface.OnClickListener negBtnListner,
                                                      int nuetBtnText, DialogInterface.OnClickListener nuetBtnListner) {
        alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setIcon(icon);
        alertDialogBuilder.setCancelable(cancelable);
        alertDialogBuilder.setPositiveButton(posBtnText, posBtnListner);
        alertDialogBuilder.setNegativeButton(negBtnText, negBtnListner);
        alertDialogBuilder.setNeutralButton(nuetBtnText, nuetBtnListner);
        alertDialogBuilder.show();

        return alertDialogBuilder;
    }
    /**
     * This can return time and date according to the parameter passed.
     * @param timeOrDate 0 for time and 1 for date
     * @return String
     * @author HAMAD SHAIKH
     * @since  19 April 2017
     */

    public static String getTimeOrDate(long timeOrDate){
        SimpleDateFormat simpleDateFormat;
        if(timeOrDate==0){
            simpleDateFormat=new SimpleDateFormat(
                    "h:mm a", Locale.getDefault());
            return simpleDateFormat.format(new Date());
        }else{
            simpleDateFormat=new SimpleDateFormat(
                    "MM/dd/yyyy", Locale.getDefault());
            return simpleDateFormat.format(new Date());
        }
    }
    public static String getDate(Date date){
        SimpleDateFormat simpleDateFormat;
            simpleDateFormat=new SimpleDateFormat(
                    "MM/dd/yyyy", Locale.getDefault());
            return simpleDateFormat.format(date);

    }
    public static String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

    public static ProgressDialog showProgressDialog(Context context, String msg, int style) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(msg);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(style);
        pDialog.show();
        return pDialog;
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
