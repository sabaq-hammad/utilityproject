package com.sabaq.utilityproject.helper;

import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.regex.Pattern;

public class Validation {

	// Regular Expression
	// you can change the expression based on your need
	public String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public String PHONE_REGEX = "\\d{4}-\\d{7}";
	public String DATE_REGEX = "\\d{2}/\\d{2}/\\d{4}";
	public String CNIC_REGEX = "\\d{5}-\\d{7}-\\d{1}";
	public String PIN_REGEX = "\\d{4}";

	// Error Messages
	public String REQUIRED_MSG = "Required";
	public String PROPER_MSG = "Enter greater than ";
	public String EMAIL_MSG = "Invalid Email Format";
	public String PHONE_MSG = "####-#######";
	public String CNIC_MSG = "#####-#######-#";
	public String DATE_MSG = "DD/MM/YYYY";
	public String PIN_MSG = "4digits";

	// call this method when you need to check email validation
	public boolean isCNIC(EditText editText, boolean required) {
		return isValid(editText, CNIC_REGEX, CNIC_MSG, required);
	}
	public boolean isEmailAddress(EditText editText, boolean required) {
		return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
	}
	public boolean isNumeric(String str)
	{
		try
		{
			int d = Integer.parseInt(str);
		}
		catch(NumberFormatException nfe)
		{
			return false;
		}
		return true;
	}
	public boolean isPin(EditText editText, boolean required) {
		return isValid(editText, PIN_REGEX, PIN_MSG, required);
	}
	
	public boolean isDate(EditText editText, boolean required) {
		return isValid(editText, DATE_REGEX, DATE_MSG, required);
	}

	// call this method when you need to check phone number validation

	public boolean isPhoneNumber(EditText editText, boolean required) {
		return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
	}

	public boolean isEditText(EditText editText, String regex, String msg, boolean required) {
		return isValid(editText, regex, msg, required);
	}

	// return true if the input field is valid, based on the parameter passed
	public boolean isValid(EditText editText, String regex,
			String errMsg, boolean required) {

		String text = editText.getText().toString().trim();
		// clearing the error, if it was previously set by some other values
		editText.setError(null);

		// text required and editText is blank, so return false
		if (required && !hasText(editText))
			return false;

		// pattern doesn't match so returning false
		if (required && !Pattern.matches(regex, text)) {
			editText.setError(errMsg);
			return false;
		}
		;

		return true;
	}

	// check the input field has any text or not
	// return true if it contains text otherwise false
	public boolean hasText(EditText editText) {

		String text = editText.getText().toString().trim();
		editText.setError(null);

		// length 0 means there is no text
		if (text.length() == 0) {
			editText.setError(REQUIRED_MSG);
			return false;
		}

		return true;
	}

	/**
	 * This method validates text field and its lenght should be greater than given length
	 * @param editText
	 * @param lenght
	 * @return boolean
	 */
	public boolean hasText(EditText editText,int lenght) {

		String text = editText.getText().toString().trim();
		editText.setError(null);

		// length 0 means there is no text
		if (text.length() == 0) {
			editText.setError(REQUIRED_MSG);
			return false;
		}else if(text.length()<lenght) {
			editText.setError(PROPER_MSG+lenght+" characters");
			return false;
		}
	return true;
	}

	/**
	 *  this will check radio group that is any option selected or not
	 * @param radioGroup
	 * @return boolean
	 */
	public boolean isChecked(RadioGroup radioGroup) {
		if(radioGroup.getCheckedRadioButtonId()==-1){
			RadioButton radioButton= (RadioButton) radioGroup.getChildAt(radioGroup.getChildCount()-1);
			radioButton.setError("Please select one option");
			return false;
		}else{
			RadioButton radioButton= (RadioButton) radioGroup.getChildAt(radioGroup.getChildCount()-1);
			radioButton.setError(null);
		}
		return true;
	}
}

