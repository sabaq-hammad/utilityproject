This project contains basic utilities such as Validation, internet detector, SharedPreference.
It also contains methods like showProgressDialog, showAlertDialog, getTimeOrDate, getDate, hideSoftKeyboard.



[![](https://jitpack.io/v/org.bitbucket.sabaq-hammad/utilityproject.svg)](https://jitpack.io/#org.bitbucket.sabaq-hammad/utilityproject)